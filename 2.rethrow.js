const http = require('http')

const options = {
	protocol: 'http:',
	hostname: 'localhost',
	path: '/fn',
	method: 'POST',
	timeout: 2000,
	port: 42001,
	headers: {
		'Content-Type': 'text/plain',
	},
}

function sendToVsCode(e) {
	const req = http.request(options, response => {
		throw e
	})
	req.write(JSON.stringify(e.toString()))
	req.end()
}

try {
	throw 'error'
} catch(e) {
	sendToVsCode(e)
}
// continue until the response
// message come from l.17 not from l.24