const http = require('http')
const options = {
	protocol: 'http:',
	hostname: 'localhost',
	path: '/fn',
	method: 'POST',
	timeout: 2000,
	port: 42001,
	headers: {
		'Content-Type': 'text/plain',
	},
}

function sendToVsCode(body) {
	const req = http.request(options)
    req.write(body)
	req.end() // request go
}
 
sendToVsCode(decodeURIComponent(process.argv[2]))