const http = require('http')

process.on('uncaughtException', async (e) => {
	await sendToVsCode(e.toString())
	console.error(e.stack)
})

const options = {
	protocol: 'http:',
	hostname: 'localhost',
	path: '/fn',
	method: 'POST',
	timeout: 2000,
	port: 42001,
	headers: {
		'Content-Type': 'text/plain',
	},
}

function sendToVsCode(body) {
	const req = http.request(options)
    req.write(JSON.stringify(body))
	req.end()
}

sendToVsCode('allo')
throw new Error('error')

// Not the user output 