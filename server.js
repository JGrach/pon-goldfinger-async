// nodemon debug-42000.js
// then run goldfinger

// alternative usage
// JSON=1 node debug-42000.js | jq .name

const http = require('http')

const port = 42001

const requestHandler = (req, res) => {
	if (req.method === 'POST') {
		let body = '';
		req.on('data', chunk => {
			body += chunk.toString(); // convert Buffer to string
		});
		req.on('end', () => {
			hasData(req, JSON.parse(body))
			res.setHeader('Access-Control-Allow-Origin', '*')
			res.end('ok');
		});
	}
	else {
		res.end()
	}
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
	if (err) {
		return console.log('something bad happened', err)
	}

	if (!process.env.JSON) {
		console.log(`server is listening on ${port}`)
	}
})


function hasData(req, body) {
	if (process.env.JSON) {
		console.log(JSON.stringify(body, null, 4))
	} else {
		console.log('------------------------------')
		console.log(body)
	}
}
